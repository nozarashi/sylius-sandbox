#!/bin/bash

set -xe

curl -sL https://deb.nodesource.com/setup_16.x | bash -

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

apt update -yqq
apt install -yqq --no-install-recommends nodejs yarn
