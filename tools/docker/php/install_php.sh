#!/bin/bash

set -xe

apt-get update && apt-get install -y \
  wget \
  curl \
  git

apt-get install -y libzip-dev libicu-dev zip unzip && docker-php-ext-install pdo zip intl opcache exif

# APCU
mkdir -p /usr/src/php/ext/apcu; \
	curl -fsSL https://pecl.php.net/get/apcu --ipv4 | tar xvz -C "/usr/src/php/ext/apcu" --strip 1; \
	docker-php-ext-install apcu

# PostgreSQL
apt-get install -y libpq-dev && docker-php-ext-install pgsql pdo_pgsql

# MySQL
docker-php-ext-install mysqli pdo_mysql

# Imagick
apt-get install -y libmagickwand-dev --no-install-recommends && pecl install imagick && docker-php-ext-enable imagick

# gd
apt-get install -y libfreetype6-dev \
  libjpeg-dev \
  libpng-dev \
  libwebp-dev
docker-php-ext-configure gd --with-jpeg --with-webp --with-freetype && docker-php-ext-install gd

# Redis
mkdir -p /usr/src/php/ext/redis; \
	curl -fsSL https://pecl.php.net/get/redis --ipv4 | tar xvz -C "/usr/src/php/ext/redis" --strip 1; \
	docker-php-ext-install redis

# xsl (required by PHPStan)
apt-get install -y libxslt-dev && docker-php-ext-install xsl

# Composer
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer

# Symfony tool
echo 'deb [trusted=yes] https://repo.symfony.com/apt/ /' | tee /etc/apt/sources.list.d/symfony-cli.list
apt update
apt install symfony-cli
