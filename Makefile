.PHONY: help

.DEFAULT_GOAL := help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

PHP_VERSION ?= 8.1
EXEC_ROOT       ?= docker-compose exec -T php
EXEC        ?= docker-compose exec --user=www-data php
EXEC_NODE ?= docker-compose run --rm --user=node node
PHP         = $(EXEC) php
SYMFONY     = $(PHP) bin/console
SYMFONY_TEST = $(PHP) bin/console -e test
COMPOSER    = $(EXEC) composer
UID         ?= 1000
GID         ?= 1000

BEHAT ?= $(PHP) ./vendor/bin/behat --format progress

DB_NAME = main

##
## Project
## -------
##

#######
.PHONY: install stop install_app version node_modules
#######

install: docker_install install_app db permissions ## Install and start the project

stop: docker_stop ## Stop the project


install_app: install_vendor node_modules version ## Install and start the project for CI

node_modules:
	$(MAKE) --no-print-directory permissions
	$(EXEC_NODE) yarn install --force
	$(EXEC_NODE) yarn build

encore:
	$(EXEC_NODE) yarn dev
watch:
	$(EXEC_NODE) yarn watch

version: ## Displays Symfony version
	${SYMFONY} --version

.PHONY: db db_create migrate generate_migration
db: db_create migrate
db_create:
	$(SYMFONY) doctrine:database:drop --if-exists --force
	$(SYMFONY) doctrine:database:create --if-not-exists

migrate:
	$(SYMFONY) doctrine:mi:migrate -n --allow-no-migration

generate_migration:
	$(SYMFONY) doctrine:mi:diff

load_fixtures:
	$(SYMFONY) doctrine:fixtures:load --no-interaction --append

sylius_fixtures:
	$(SYMFONY) sylius:fixtures:load app -n

##
## Docker
## ------
##

#######
.PHONY: docker_install docker_build docker_up docker_stop
#######

docker_build:
	docker-compose build --pull --build-arg UID=$$(id -u) --build-arg GID=$$(id -g) --build-arg PHP_VERSION=$(PHP_VERSION)

docker_up: ## Compose Image docker
	docker-compose up -d

docker_stop:
	docker-compose stop

docker_down:
	docker-compose down --remove-orphans -v

docker_rmi:
	docker-compose down --remove-orphans --rmi all -v

docker_logs:
	docker-compose logs --tail=0 --follow

docker_install: docker_build docker_up ## Install docker

##
## Utils
## -----
##

shell: ## Starts a bash shell as www-data on the docker PHP machine
	docker-compose exec --user=www-data php bash

##
## Tests
## -----
##

fix: vendor ## Launch php-cs-fixer
	$(PHP) ./vendor/bin/php-cs-fixer fix src/ tests/ --config=.php_cs.dist --allow-risky=yes

phpcs-fixer:
	$(PHP) ./vendor/bin/php-cs-fixer fix src/ tests/ --config=.php_cs.dist --allow-risky=yes --dry-run -v



# rules based on files
composer.lock: ## Generate composer.lock
	$(COMPOSER) update --lock --no-scripts --no-interaction;

install_vendor: composer.json ## Install vendor
	$(COMPOSER) install --optimize-autoloader

.PHONY: composer_update
composer_update: ##Update vendor
	$(COMPOSER) update -W

cc:
	$(SYMFONY) cache:clear

permissions: ## Fix file permissions for project
	$(EXEC_ROOT) chown -R $$(id -u):$$(id -g) *;
	$(EXEC_ROOT) chown -R $$(id -u):$$(id -g) /home/www-data;
	$(EXEC_ROOT) chown -R $$(id -u):$$(id -g) /srv/app;
	$(EXEC_ROOT) chown -R $$(id -u):$$(id -g) /tmp;

test_db: SYMFONY = $(SYMFONY_TEST)
test_db: DB_NAME = main_test
test_db: db

tests: phpunit behat

behat:
	$(MAKE) --no-print-directory test_db
ifdef BEHAT_TAGS
	$(BEHAT) --tags=$(BEHAT_TAGS)
else
	$(BEHAT)
endif

phpqa:
	$(PHP) ./vendor/bin/phpqa

pre_commit: fix phpqa tests

phpunit:
	$(MAKE) --no-print-directory test_db
	$(PHP) bin/phpunit

messenger_consume:
	$(SYMFONY) messenger:consume async -vv


rector:
	vendor/bin/rector process

.PHONY: generate_certs
generate_certs:
	mkcert -cert-file tools/docker/caddy/certs/tls.pem -key-file tools/docker/caddy/certs/tls.key "sylius.localhost"
